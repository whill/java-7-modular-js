/**
 * 
 */
package com.mjse.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author winfieldhill
 * @web.servlet
 *   name=Hello
 */
@WebServlet( "/Category" )
public class CategoryPage extends HttpServlet {

	/**
	 * 3.0 implementation Servlet request forwarding PRE<GET> and POST processing.
	 */
	private static final long serialVersionUID = 1L;

	
	@Override
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		Map<String, String> messages = new HashMap<String, String>();
		
		request.setAttribute( "helloMessages", messages );
		messages.put( "firstName", "Hill" );
		request.getRequestDispatcher( "/views/index.jsp" ).forward( request, response );
	}
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		Map<String, String> messages = new HashMap<String, String>();
		
		request.setAttribute( "helloMessages", messages );
		messages.put( "firstName", "Hill2" );
		
		request.getRequestDispatcher( "/views/index.jsp" ).forward( request, response );
	}
}