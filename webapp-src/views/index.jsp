<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Java 7 Stack with Modular JS front-end &amp; CSS3</title>
		<link rel="stylesheet" href="css/main.css" />
		<script data-main="js/main" src="js/lib/require.js"></script>
	</head>
	<body>
		<c:out value="${ helloMessages.firstName }" />
	</body>
</html>