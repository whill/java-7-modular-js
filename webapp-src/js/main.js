require.config( {
	    paths: {
	        'threeJS': 'lib/three',
			'jquery': [
			            '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min',
			            /* If the CDN location fails, load from this location */
			            'lib/jquery-2.1.0.min'
					],
			'OrbitControls' : 'lib/OrbitControls',
			'cTM' : 'amd/completeThreeModule'
	    },
	    shim : {
	    	'OrbitControls' : {
	    		deps : ['threeJS'],
	    		exports : 'THREE'
	    	}
	    }
} );

require( ['cTM'], function( cTM ) {
	cTM.scene1();
} );