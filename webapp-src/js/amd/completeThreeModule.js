/**
 * 
 */
define( ['OrbitControls'], function( threeJS ) {
	console.log( threeJS );
	return {
		scene1: function() {
			

			var
				scene = new threeJS.Scene(),
				camera = new threeJS.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 ),
				renderer = new threeJS.WebGLRenderer(),
				geometry = new threeJS.CubeGeometry( 200,  200, 200 ),
				material = new threeJS.MeshLambertMaterial(
						{
							color: 0xE3002A
						}
				),
				cube = new threeJS.Mesh( geometry, material ),
				light = new threeJS.DirectionalLight( 0xffffff ),
				ambientLight = new threeJS.AmbientLight( 'blue' ),
				controls = new threeJS.OrbitControls( camera );

			cube.overdraw = true;

			light.position.set( 1, 1, 1 );
			
			controls.autoRotate = true;
			controls.autoRotateSpeed = 1;

			camera.position.z = 500;

			renderer.setSize( window.innerWidth, window.innerHeight );
			document.body.appendChild( renderer.domElement );
			
			scene.add( cube );
			scene.add( light );
			scene.add( ambientLight );

			
			function render() {
				requestAnimationFrame( render );
				
				controls.update();
				cube.rotation.x += 0.005;
				cube.rotation.y += 0.001;
				
				renderer.render( scene, camera );
			};
			render();
		}
	};
} );